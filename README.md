# Csi
**Instructions:** \
npm i\
npm run server

**Notes:** \
Mockserver is having issues , npm run server didnt work as it is,  so I just copied the file. If u want, after a node js server or any npm service is raised to expose the json, the fetch function for a http request instead the local file would be:

```
  componentWillMount() {
    let { state, updateValue } = this.context;
    if (state.puppies !== null) {
      this.setState({
        puppies: state.puppies
      }, () => {
        updateValue("puppies", this.state.puppies);
      });
    } else {
      fetch('http://localhost:8080/')
        .then(response => response.json())
        .then(data => this.setState({ puppies: data.puppies }),
          () => {
            updateValue("puppies", this.state.puppies);
          });

    }
  }
```

inside Dogs.tsx to make it work.



