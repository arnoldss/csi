
import React from "react";
import puppie from "../../models/interface";
import Grid from '@material-ui/core/Grid';
import MyContext from "../../context/context";
import Button from '@material-ui/core/Button';
import CardPuppieCheckOut from "../CardPuppieCheckOut/CardPuppieCheckOut";
import "./CheckOut.css";

class CheckOut extends React.Component {
  static contextType = MyContext;

  submitApplication = () => {
    let { state, updateValue } = this.context;
    let puppiesAdopted: puppie[] = state.puppies && state.puppies.filter((puppie: puppie) => {
      if (puppie.pendingStatus === true) {
        puppie.adoptedStatus = true;
        puppie.pendingStatus = false;
      }
      return puppie;
    });
    updateValue("puppies", puppiesAdopted);

  }

  render() {

    let { state, updateValue } = this.context;
    let puppiesPreAdoption: puppie[] = state.puppies && state.puppies.filter((puppie: puppie) => {
      return puppie.pendingStatus === true;
    });


    return (
      <div> 
         { puppiesPreAdoption && puppiesPreAdoption.length > 0 &&
        <h2> Your New Friends! </h2>
         }
        <Grid
          container
          spacing={2}
          direction="column"
          justify="flex-start"
          alignItems="flex-start"
        >
          {puppiesPreAdoption && puppiesPreAdoption.map((x, i) => {
            return <Grid item xs={6} sm={6} md={6} key={i}>
              <CardPuppieCheckOut
                title={x.title}
                url={x.url}
                id={x.id}
              ></CardPuppieCheckOut>
            </Grid>
          })}
        </Grid>
        { puppiesPreAdoption && puppiesPreAdoption.length > 0 &&
        <div className={"csi-button-checkout"}> 
        <Button variant="contained" size="small" color="secondary"
                    onClick={() => this.submitApplication()} >

          Submit Adoption
        </Button>
        </div>
    }
      </div>
    );
  }
}

export default CheckOut;
