
import React from "react";
import CardPuppie from "../CardPuppie/CardPuppie";
import puppies from "./puppies";
import puppie from "../../models/interface";
import Grid from '@material-ui/core/Grid';
import MyContext from "../../context/context";



const puppiesData = puppies;
interface IDogs {
  loading?: boolean;
  puppies?: puppie[] | null;

}


class Dogs extends React.Component<any, IDogs> {
  static contextType = MyContext;

  constructor() {
    super(null, null);
    this.state = {
      loading: false,
      puppies: null,
    };
  }

  componentWillMount() {    
    let { state, updateValue } = this.context;
      this.setState({
        puppies: state.puppies !== null ? state.puppies : puppiesData[0].data
      }, () => {
        updateValue("puppies", this.state.puppies);
      });
  }

  render() {

    const { state, updateValue } = this.context;
    const puppiesNotSelected: puppie[] = state.puppies && state.puppies.filter((puppie: puppie)=>{
      return puppie.adoptedStatus !== true && puppie.pendingStatus !== true;
  
  });
    return (
      <div>
        <Grid
          container
          spacing={2}
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
        >
          {puppiesNotSelected && puppiesNotSelected.map((x, i) => {
            return <Grid item xs={12} sm={6} md={3} key={i}>
              <CardPuppie
                title={x.title}
                url={x.url}
                id={x.id}
              ></CardPuppie>
            </Grid>
          })}
        </Grid>
      </div>
    );
  }
}

export default Dogs;
