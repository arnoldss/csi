
import React from "react";
import rick from './rick.png';

class NotFound extends React.Component {

    render() {
      return (
        <div>
        <img src={rick} width="90%" height="auto" >
        </img>
      </div>
      );
    }
  }
  export default NotFound;
   