
import React from "react";
import CardPuppie from "../CardPuppie/CardPuppie";
import puppie from "../../models/interface";
import Grid from '@material-ui/core/Grid';
import MyContext from "../../context/context";

class Adoptions extends React.Component {
  static contextType = MyContext;

  render() {
    const { state, updateValue } = this.context;
    const puppiesAdopted: puppie[] = state.puppies && state.puppies.filter((puppie: puppie)=>{
      return puppie.adoptedStatus === true;
  
  });
    return (
      <div>
         { puppiesAdopted && puppiesAdopted.length > 0 &&
<h2> Your Companions! </h2>
  }
        <Grid
          container
          spacing={2}
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
        >
          {puppiesAdopted && puppiesAdopted.map((x, i) => {
            return <Grid item xs={12} sm={6} md={3} key={i}>
              <CardPuppie
                title={x.title}
                url={x.url}
                id={x.id}
              ></CardPuppie>
            </Grid>
          })}
        </Grid>
      </div>
    );
  }
}

export default Adoptions;
