import React, { useContext } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import "./CardPuppieCheckOut.css";
import MyContext from "../../context/context";
import puppie from "../../models/interface";
import { red } from '@material-ui/core/colors';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

const CardPuppieCheckOut = (puppie: puppie) => {
  const { state, updateValue } = useContext(MyContext);
  const redTheme = createMuiTheme({ palette: { primary: red } })

  const deletePuppie = (id: any) => {
    state.puppies.filter((puppie: puppie) => {
      if (puppie.id === id) {
        puppie.pendingStatus = false;
      }
    });
    console.log(state)
    updateValue("puppies", state.puppies);
  }

  return (
    <Card className={"csi-card-checkout"} >
      <CardActionArea>
        <CardContent>
          <div className={"csi-card-content"}>
          <div>
            <img src={puppie.url} width="100" height="70" >
            </img>
          </div>
          <div>
          <Typography variant="body2" color="secondary" component="p">
            {puppie.title}
          </Typography>
          </div>
          <div>
          <MuiThemeProvider theme={redTheme}>

          <Button variant="contained" size="small" color="primary"
            onClick={() => deletePuppie(puppie.id)} >
            Delete
          </Button>
          </MuiThemeProvider>
          </div>

          </div>
        </CardContent>
      </CardActionArea>


    </Card>
  )
}



export default CardPuppieCheckOut;