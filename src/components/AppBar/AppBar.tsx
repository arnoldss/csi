
import React, { useContext } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { withRouter } from "react-router-dom";
import "./appbar.css"
import MyContext from '../../context/context';
import { useHistory } from "react-router-dom";
import puppie from "../../models/interface";
import {RouteComponentProps} from "react-router";


const ButtonAppBar = () => {
    const { state, updateValue } = useContext(MyContext);
    const history = useHistory();
    const count: puppie[] = state.puppies && state.puppies.filter((puppie: puppie) => {
      return puppie.pendingStatus === true;
    });

    
    return (
      <>
        <AppBar key={1} position="static" color="primary">
          <Toolbar>


            <div className={"csi-appbar"}>
              <div >
                <Button type="submit" color="secondary"
                  onClick={() => {
                    history.push("/dogs");

                  }}
                >

                  Dogs

                </Button>
              </div>
              <div >
                <Button type="submit" color="secondary"
                  onClick={() => {
                    history.push("/adoptions");
                  }}
                >
                  Adoptions
                </Button>
              </div>
              <div className={"csi-appbar-checkout"}>
                <Button type="submit" color="secondary"
                  onClick={() => {
                    history.push("/check-out");
                  }}
                >
                  Checkout ( {  count !== null && count.length ? count.length : "0"  } )
                </Button>

              </div>
            </div>
          </Toolbar>

        </AppBar>
      </>
    );
  
}

export default withRouter(ButtonAppBar);