import React, { useContext } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import "./cardPuppies.css";
import MyContext from "../../context/context";
import puppie from "../../models/interface";
import { useLocation, withRouter } from "react-router-dom";

const CardPuppie = (puppie: puppie) => {
  const { state, updateValue } = useContext(MyContext);
  const location = useLocation();


  const preAdoptPuppie = (id: any) => {
    state.puppies.filter((puppie: puppie) => {
      if (puppie.id === id) {
        puppie.pendingStatus = true;
      }

    })

    console.log(state)
    updateValue("puppies", state.puppies);
  }

  return (
    <Card className={"csi-card"} >
      <CardActionArea>
        <CardMedia style={{ height: 0, paddingTop: '56%' }}
          image={puppie.url}
          title={puppie.title}
        />
        <CardContent>
          <Typography variant="body2" color="secondary" component="p">
            {puppie.title}
          </Typography>
        </CardContent>
      </CardActionArea>
      {location.pathname === '/dogs' &&
        <CardActions>
          <Button variant="contained" size="small" color="secondary"
            onClick={() => preAdoptPuppie(puppie.id)} >
            Adopt
          </Button>
        </CardActions>
      }
    </Card>
  )
}



export default CardPuppie;