import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  BrowserRouter

} from "react-router-dom";
import "./App.css";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import { createMuiTheme } from "@material-ui/core/styles";

import Dogs from './components/Dogs/Dogs';
import Adoptions from './components/Adoptions/Adoptions';
import NotFound from './components/NotFound/NotFound';
import CheckOut from './components/CheckOut/CheckOut';
import AppBar from './components/AppBar/AppBar';

import MyContext from "./context/context";


const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#e0ffff",
    },
    secondary: {
      main: "#648dae",
    },

  },
});

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      puppies: null,
      loading: false,

    };
  }


  componentWillMount () {
    this.setState({
      puppies: JSON.parse(localStorage.getItem("puppies")) !== null ? JSON.parse(localStorage.getItem("puppies")) : null,
    })

    

  }

  updateValue = (key, val) => {
    this.setState({ [key]: val }, 
      () =>   { 
      localStorage.setItem("puppies", JSON.stringify(this.state.puppies))
    }
    );

  };


  render() {
    return (
<MyContext.Provider
        value={{ state: this.state, updateValue: this.updateValue }}
      >
      <MuiThemeProvider theme={theme}>
        <React.Fragment>

          <BrowserRouter>
            <div className="App-Bar">
              <AppBar />
              {this.state && this.state.loading && (
                <LinearProgress color="secondary" />
              )}
              <div
                className="App-Content" >
                <Switch>
                  <Route exact path="/dogs" component={Dogs} />
                  <Route exact path="/adoptions" component={Adoptions} />
                  <Route exact path="/not-found" component={NotFound} />
                  <Route exact path="/check-out" component={CheckOut} />

                  <Redirect exact from="/" to="/dogs" />
                  <Redirect from="*" to="/not-found" />
                </Switch>

              </div>
            </div>
          </BrowserRouter>

        </React.Fragment>

      </MuiThemeProvider>
      </MyContext.Provider>

    );
  }
}
export default App;
