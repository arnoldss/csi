import { PerformanceNodeTiming } from "perf_hooks";

interface puppie {
    id?: string,
    url?: string,
    title?: string,
    adoptedStatus?: boolean,
    pendingStatus?: boolean,


}

export default puppie;